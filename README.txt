INTRODUCTION
------------
Mailblock is a module for preventing drupal_mail() messages sent from
development and staging sites from going to undesirable addresses. This is a
good safety net, provided the site does actually send all its mail through
drupal_mail -- the module will actually check that for you when you run the
Drupal status report, and you should follow up any Mailblock warnings on that
report as a matter of importance.

REQUIREMENTS
------------
"No special requirements".

INSTALLATION
------------
Install as you would normally install a contributed Drupal module. 
See: https://drupal.org/documentation/install/modules-themes/modules-7 
for further information.

CONFIGURATION
-------------

 * Configure user permissions in Administration � People � Permissions:

   - Administer Mailblock

     Required to configure Mailblock settings.

 * Customize Mailblock settings in 
 
   - Administration � Configuration � Development � Mailblock

MAINTAINERS
-----------

Current maintainers:
 * Levi Wilson-Brown (Signify-Levi) - https://drupal.org/user/3329019
