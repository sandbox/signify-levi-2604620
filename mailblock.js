/**
 * @file
 * Provides JavaScript additions to the Mailblock module.
 */

Drupal.behaviors.mailblockBehavior = {
  attach: function (context) {
    "use strict";
    (function ($) {
      // Hide the 'white list' and 'deliver to site domain' fieldsets
      // if the 'force' option is enabled.
      var options = $('.form-item-mailblock-whitelist, .form-item-mailblock-deliver-to-site-domain');
      if ($('#edit-mailblock-force').attr('checked')) {
        options.hide();
      }
      $('#edit-mailblock-force').change(function () {
        options.toggle('fast');
      });
    })(jQuery);
  }
};
